import {
  AfterContentInit,
  Component,
  ContentChild, Directive,
  Input,
  OnInit,
  ViewContainerRef
} from '@angular/core';

// tslint:disable-next-line:directive-selector
@Directive({selector: 'pane'})
// tslint:disable-next-line:directive-class-suffix
export class Pane implements OnInit, AfterContentInit {
  @Input() id !: string;
  @ContentChild(Pane, {static: false, read: ViewContainerRef}) portal;
  private window = null;

  constructor() {
  }

  ngAfterContentInit() {
    console.log(this.window);
    console.log(this.portal);
    this.window.document.body.appendChild(this.portal.element.nativeElement);
    this.window.document.body.appendChild(this.portal.element.nativeElement);
  }

  ngOnInit(): void {
    this.window = window.open('', 'strWindowName ', 'width=600,height=400,left=200,top=200');
  }
}


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements AfterContentInit {

  ngAfterContentInit() {
  }
}
